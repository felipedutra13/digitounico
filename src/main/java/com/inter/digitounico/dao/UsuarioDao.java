package com.inter.digitounico.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.inter.digitounico.model.Usuario;

@Repository
public interface UsuarioDao extends JpaRepository<Usuario, Long>{

	Usuario findById(Integer id);
	
	Usuario findByEmail(String email);
}
