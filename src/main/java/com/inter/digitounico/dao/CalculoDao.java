package com.inter.digitounico.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.inter.digitounico.model.Resultado;

@Repository
public interface CalculoDao extends JpaRepository<Resultado, Long>{

	
}