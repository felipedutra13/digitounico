package com.inter.digitounico.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.google.gson.Gson;
import com.inter.digitounico.dao.UsuarioDao;
import com.inter.digitounico.model.Usuario;

@RestController
@EnableWebMvc
@RequestMapping(value = "/usuario")
public class UsuarioController {

	@Autowired
	private UsuarioDao usuarioDao;
	
	// LER
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Usuario>> getUsuarios() {
		List<Usuario> usuarios = usuarioDao.findAll();
		if(usuarios == null || usuarios.isEmpty())
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		return new ResponseEntity<>(usuarios, HttpStatus.OK);
	}
	
	// CRIAR
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Usuario> criarUsuario(@RequestBody String payload) {
		Usuario usuario = new Gson().fromJson(payload, Usuario.class);
		usuarioDao.save(usuario);
		return new ResponseEntity<>(usuario, HttpStatus.CREATED);
	}
	
	// DELETAR
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Usuario> deletarUsuario(@PathVariable String id) {
		Usuario usuario = usuarioDao.findById(Integer.parseInt(id));
		if(usuario == null)
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		usuarioDao.delete(usuario);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	// ATUALIZAR
	@RequestMapping(value = "/{id}", method = RequestMethod.POST)
	public ResponseEntity<Usuario> atualizarUsuario(@PathVariable String id, @RequestBody String payload) {
		Usuario usuarioBase = usuarioDao.findById(Integer.parseInt(id));
		if(usuarioBase == null)
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		Usuario usuario = new Gson().fromJson(payload, Usuario.class);
		usuarioDao.saveAndFlush(atualizarAtributos(usuarioBase, usuario));
		return new ResponseEntity<>(usuario, HttpStatus.OK);
	}
	
	
	// faz o merge dos atributos
	private Usuario atualizarAtributos(Usuario usuarioBase, Usuario atualizarUsuario)
	{
		if(atualizarUsuario.getNome() == null)
			atualizarUsuario.setNome(usuarioBase.getNome());
		if(atualizarUsuario.getEmail() == null)
			atualizarUsuario.setEmail(usuarioBase.getEmail());
		atualizarUsuario.setResultados(usuarioBase.getResultados());
		atualizarUsuario.setId(usuarioBase.getId());
		return atualizarUsuario;
	}
}