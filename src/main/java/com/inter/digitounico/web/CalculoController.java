package com.inter.digitounico.web;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.inter.digitounico.dao.CalculoDao;
import com.inter.digitounico.dao.UsuarioDao;
import com.inter.digitounico.model.Resultado;
import com.inter.digitounico.model.Usuario;

@RestController
@RequestMapping(value = "/calculo")
public class CalculoController {

	// Controlar cache
	List<Resultado> cache = new ArrayList<>();
	int count = 0;
	//////////////

	@Autowired
	private CalculoDao calculoDao;

	@Autowired
	private UsuarioDao usuarioDao;

	// CREATE
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Resultado> calcularDigito(@RequestBody String payload) {
		Resultado resultado = new Gson().fromJson(payload, Resultado.class);
		resultado.setResultado(digitoUnico(resultado));
		if (resultado.getUsuario() != null) {
			resultado.setUsuario(buscaUsuario(resultado.getUsuario()));
		}
		calculoDao.save(resultado);
		return new ResponseEntity<>(resultado, HttpStatus.CREATED);
	}

	// READ
	@RequestMapping(value = "/usuario/{id}", method = RequestMethod.GET)
	public ResponseEntity<List<Resultado>> getCalculosPorUsuario(@PathVariable String id) {
		Usuario usuario = usuarioDao.findById(Integer.parseInt(id));
		if (usuario == null)
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		return new ResponseEntity<>(usuario.getResultados(), HttpStatus.OK);
	}

	private static int calcular(String n, int k) {
		int total = 0;
		for (int i = 0; i < n.length(); i++) {
			total += Character.getNumericValue(n.charAt(i));
		}
		total *= k;
		if (total >= 10) {
			String newN = String.valueOf(total);
			total = calcular(newN, 1);
		}
		return total;
	}

	private Usuario buscaUsuario(Usuario usuario) {
		return usuarioDao.findByEmail(usuario.getEmail());
	}

	private int digitoUnico(Resultado resultado) {

		// Varre a lista procurando o calculo pelos parametros
		for (int i = 0; i < cache.size(); i++) {
			Resultado fromCache = cache.get(i);
			if (fromCache.getK() == resultado.getK() && fromCache.getN().equals(resultado.getN())) {
				return fromCache.getResultado();
			}
		}
		
		resultado.setResultado(calcular(resultado.getN(), resultado.getK()));
		if(cache.size() < 10)
			cache.add(resultado);
		else {
			cache.set(count, resultado);
			count++;
			if(count == 10)
				count = 0;
		}

		return resultado.getResultado();
	}
}
