package com.inter.digitounico.web;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.inter.digitounico.dao.UsuarioDao;
import com.inter.digitounico.model.Usuario;

@RunWith(SpringRunner.class)
@WebMvcTest(value = UsuarioController.class)
public class UsuarioControllerTest {

    @Autowired
    WebApplicationContext webApplicationContext;
    
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private UsuarioDao usuarioDao;

	@Before
	public void setup() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}
	
    @Test
    public void criarUsuarioSucesso() throws Exception {
    	
    	String payload = "{\r\n" + 
    			"    \"nome\": \"felipe\",\r\n" + 
    			"    \"email\": \"felipe@email.com\"\r\n" + 
    			"}";

        mockMvc.perform(post("/usuario")
                .content(payload)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }
    
    @Test
    public void lerUsuarioSucesso() throws Exception {
    	when(usuarioDao.findAll()).thenReturn(Collections.singletonList(new Usuario()));
        mockMvc.perform(get("/usuario")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
    
    @Test
    public void lerUsuarioNaoEncontrado() throws Exception {
    	when(usuarioDao.findAll()).thenReturn(Collections.emptyList());
        mockMvc.perform(get("/usuario")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
    
    @Test
    public void deletarUsuarioSucesso() throws Exception {
    	Usuario usuario = new Usuario();
    	usuario.setId(1);
    	usuario.setNome("Felipe");
    	usuario.setEmail("felipe@email.com");
    	
    	when(usuarioDao.findById(1)).thenReturn(usuario);
    	
        mockMvc.perform(delete("/usuario/1")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }
    
    @Test
    public void deletarUsuarioNaoEncontrado() throws Exception {    	
    	when(usuarioDao.findById(1)).thenReturn(null);
    	
        mockMvc.perform(delete("/usuario/1")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
    
    @Test
    public void atualizarUsuarioSucesso() throws Exception {    	
    	Usuario usuario = new Usuario();
    	usuario.setId(1);
    	usuario.setNome("Felipe");
    	usuario.setEmail("felipe@email.com");
    	
    	when(usuarioDao.findById(1)).thenReturn(usuario);
    	
    	String payload = "{\r\n" + 
    			"    \"nome\": \"joao\",\r\n" + 
    			"    \"email\": \"joao@email.com\"\r\n" + 
    			"}";
    	
        mockMvc.perform(post("/usuario/1")
                .content(payload)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
    
    @Test
    public void atualizarUsuarioNaoEncontrado() throws Exception {    	
    	when(usuarioDao.findById(1)).thenReturn(null);
    	
        mockMvc.perform(post("/usuario/1")
                .content("{}")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}
