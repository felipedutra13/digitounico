package com.inter.digitounico.web;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.google.gson.Gson;
import com.inter.digitounico.dao.CalculoDao;
import com.inter.digitounico.dao.UsuarioDao;
import com.inter.digitounico.model.Resultado;
import com.inter.digitounico.model.Usuario;

@RunWith(SpringRunner.class)
@WebMvcTest(value = CalculoController.class)
public class CalculoControllerTest {
	
    @Autowired
    WebApplicationContext webApplicationContext;
    
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private UsuarioDao usuarioDao;
	
	@MockBean
	private CalculoDao calculoDao;

	@Before
	public void setup() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}
	
    @Test
    public void lerResultadosDeUsuarioSucesso() throws Exception {
    	Usuario usuario = new Usuario();
    	Resultado resultado = new Resultado();
    	resultado.setN("9876");
    	resultado.setK(4);
    	resultado.setResultado(8);
    	usuario.setResultados(Collections.singletonList(resultado));
    	
    	when(usuarioDao.findById(anyInt())).thenReturn(usuario);
    	
        mockMvc.perform(get("/calculo/usuario/1")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
    
    @Test
    public void lerResultadosDeUsuarioNaoEncontrado() throws Exception {   	
    	when(usuarioDao.findById(anyInt())).thenReturn(null);
    	
        mockMvc.perform(get("/calculo/usuario/1")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
    
    @Test
    public void calcularDigitoSucesso() throws Exception {
    	String payload = "{\r\n" + 
    			"    \"n\": \"4567\",\r\n" + 
    			"    \"k\": 6\r\n" + 
    			"}";
    	
        MvcResult result = mockMvc.perform(post("/calculo")
                .content(payload)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();
        Resultado response = new Gson().fromJson(result.getResponse().getContentAsString(), Resultado.class);
        assertThat(response.getResultado(), equalTo(6));
    }

}
