# Digito Único

Deﬁnimos	um	dígito	único	de	um	inteiro	usando	as	seguintes
regras:
Dado	um	inteiro,	precisamos	encontrar	o	dígito	único	do	inteiro.
Se	x	tem	apenas	um	dígito,	então	o	seu	dígito	único	é	x.
Caso	contrário,	o	dígito	único	de	x	é	igual	ao	dígito	único	da
soma	dos	dígitos	de	x.

## Compilando

- mvn install

## Rodando a aplicação

- spring-boot:run

## Testes unitários

- mvn test